import { gql } from 'apollo-server-express';

export default gql`
  extend type Query {
    attractions(offset: Int, limit: Int, filter: String): [Attraction!]
  }

  type Attraction {
    _id: ID!
    id: String!
    originalName: String!
  }
`;
