/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-useless-escape */
import { getModelForClass, prop } from '@typegoose/typegoose';

class AttractionClass {
  @prop({ 
    required: [true, "id is required."], 
    unique: true,
    validate: {
      validator(v: string) {
        return /^[\w]+?_[\w\-]+?$/.test(v);
      },
      message: 'id can only have letters, numbers, - and _., and should be ParkID_AttractionID.',
    }})
  public id!: string;

  @prop({ 
    required: [true, "originalName is required."],
    validate: {
      validator(v: string) {
        return /^[a-zA-Z0-9-_&ñ\'øå:\"!'ü+\’Öíé–\‘\. ]+$/.test(v);
      },
      message: 'originalName can only have letters, numbers, -, _ and spaces.',
    }
   })
  public originalName!: string;

  @prop({ 
    required: [true, "latitude is required."],
    validate: {
      validator(v: any) {
        if (v > 90 || v < -90) {
          return false;
        }
        return /\d{1,3}\.\d+/.test(v);
      },
      message: 'latitude should exist of (XX)X.XXXXX.',
    }
   })
  public latitude!: string;

  @prop({ 
    required: [true, "longitude is required."],
    validate: {
      validator(v: any) {
        if (v > 180 || v < -180) {
          return false;
        }
        return /\d{1,3}\.\d+/.test(v);
      },
      message: 'longitude should exist of (XX)X.XXXXX.',
    }
   })
  public longitude!: string;
}


const Attraction = getModelForClass(AttractionClass, {
  schemaOptions: {collection: 'attractions', timestamps: true}
});

export { Attraction };
