/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */

export default {
  Query: {
    attractions: (parent: any, { offset = 0, limit = 100, filter = null }: any, { Attraction }: { Attraction: any}): any => {
      if (filter === null) {
        return Attraction.find({}).skip(offset).limit(limit);
      } else {
        return Attraction.find({ originalName: { $regex: filter, $options: 'i' } });
      }
    },
  },
};
