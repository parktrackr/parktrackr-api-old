/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */

import fetch from 'node-fetch';
import dotenv from 'dotenv'
dotenv.config()

class EuropaPark {
  name: string;
  latutude: number;
  longitude: number;
  timezone: string;
  apiURL: any;
  loginURL: any;
  refreshURL: any;
  apiUsername: string | undefined;
  apiPassword: string | undefined;

  constructor() {
    this.name = 'Europa-Park';

    this.latutude = 48.266140769976715;
    this.longitude = 7.722050520358709;

    this.timezone = 'Europe/Berlin';

    this.apiURL = process.env.EUROPAPARK_API_URL;
    this.loginURL = process.env.EUROPAPARK_LOGIN_URL;
    this.refreshURL = process.env.EUROPAPARK_REFRESH_URL;

    this.apiUsername = process.env.EUROPAPARK_USER_NAME;
    this.apiPassword = process.env.EUROPAPARK_PASSWORD;
  }

  async FetchLogin(): Promise<any> {
    return fetch(this.loginURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ password: this.apiPassword, username: this.apiUsername }),
    })
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        let refresh_token = response.refresh_token;
        refresh_token = { refresh_token: refresh_token };
        return refresh_token;
      });
  }

  async FetchRefreshToken(refresh_token: any): Promise<any>  {
    return fetch(this.refreshURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(refresh_token),
    })
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        let token = response.token;
        token = 'Bearer ' + token;
        return token;
      });
  }

  async FetchPOIs(): Promise<any>  {
    const refresh_token = await this.FetchLogin();
    const refreshToken = await this.FetchRefreshToken(refresh_token);

    return fetch(this.apiURL, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        JWTAuthorization: refreshToken,
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        return response;
      });
  }

  ProcessAttractionPOI(data: any): any {
    const { id, name, latitude, longitude } = data;
    return {
      id: `europapark_${id}`,
      originalName: name,
      latitude: `${latitude}`,
      longitude: `${longitude}`,
    };
  }
}

export default EuropaPark;
