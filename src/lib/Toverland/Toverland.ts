/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import fetch from 'node-fetch';
import dotenv from 'dotenv'
dotenv.config()


class Toverland {
  name: string;
  latitude: number;
  longitude: number;
  apiAuthKey: any;
  apiHostURL: any;
  openiningHoursURL: string | undefined;

  constructor() {
    this.name = 'Toverland';

    this.latitude = 51.39704411372531;
    this.longitude = 5.984798893991941;
    // API settings
    this.apiAuthKey = process.env.TOVERLAND_AUTH_KEY;
    // URL settings
    this.apiHostURL = process.env.TOVERLAND_API_URL;
    this.openiningHoursURL = process.env.TOVERLAND_OPENINGTIMES_URL;
  }

  async FetchRidePOIs(): Promise<any> {
    return fetch(this.apiHostURL + '/park/ride/list', {
      method: 'GET',
      headers: {
        Authorization: this.apiAuthKey,
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        return data;
      })
      .catch((err) => {
        throw new Error(err);
      });
  }

  ProcessAttractionPOI(data: any): any {
    const { id, name, latitude, longitude } = data;
    return {
      id: `toverland_${id}`,
      originalName: name,
      latitude: latitude,
      longitude: longitude,
    };
  }
}

export default Toverland;

/*
List of Endpoints
'/park/ride/list'
'/park/ride/operationInfo/list'
"/park/ride/list/" + t
"/park/ride/" + t + "/status"
"/park/ride/" + t + "/times"
"/park/ride/" + t + "/waitingTimes"

'/park/foodAndDrinks/list'
'/park/foodAndDrinks/operationInfo/list'
"/park/foodAndDrinks/list/" + t
"/park/foodAndDrinks/" + t + "/status"
"/park/foodAndDrinks/" + t + "/times"

'/park/shop/list'
'/park/shop/operationInfo/list'
"/park/shop/list/" + t
"/park/shop/" + t + "/status"
"/park/shop/" + t + "/times"

'/park/show/list'
'/park/show/operationInfo/list'
"/park/show/list/" + t
"/park/show/" + t + "/times"

"/park/services/list"
"/park/services/categories"
"/park/services/list/" + t

"/park/halloween/list"
"/park/halloween/categories"
"/park/halloween/list/" + t
"/park/halloween/operationInfo/list"
"/park/halloween/operationInfo/list/" + t
"/park/halloween/" + t + "/info"
*/
