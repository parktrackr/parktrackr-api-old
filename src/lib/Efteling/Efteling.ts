/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */

import fetch from 'node-fetch';
import * as enums from './enums';
import dotenv from 'dotenv'
dotenv.config()

class Efteling {
  name: string;
  latitude: number;
  longitude: number;
  timezone: string;
  apiKey: string | undefined;
  appVersion: string | undefined;
  apiVersion: string | undefined;
  apiPlatform: string | undefined;
  searchURL: string | undefined;
  waitTimesURL: string | undefined;
  enums: typeof enums;
  previousOperations: null;
  apiAuthKey!: string;

  constructor() {
    this.name = 'Efteling';

    // location settings
    this.latitude = 51.65098350641645;
    this.longitude = 5.049916835374731;

    this.timezone = 'Europe/Amsterdam';

    // api settings
    this.apiKey = process.env.EFTELING_API_KEY;
    this.appVersion = process.env.EFTELING_APP_VERSION;
    this.apiVersion = process.env.EFTELING_API_VERSION;
    this.apiPlatform = process.env.EFTELING_API_PLATFORM;

    // URL settings
    this.searchURL = process.env.EFTELING_SEARCH_URL;
    this.waitTimesURL = process.env.EFTELING_WAITTIMES_URL;

    this.enums = enums;

    this.previousOperations = null;
  }

  async FetchPOIs(lang: string): Promise<any> {
    return fetch(
      this.searchURL +
        `search?q.parser=structured&size=1000&q=%28and%20%28phrase%20field%3Dlanguage%20%27$${lang}%27%29%29`,
      {
        method: 'GET',
        headers: {
          Authorization: this.apiAuthKey,
        },
      },
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        return data;
      })
      .catch((err) => {
        throw new Error(err);
      });
  }

  ProcessAttractionPOI(data: any): any  {
    const { fields } = data;
    const { id, name, latlon } = fields;
    const latlonSplit = latlon.split(',');

    return {
      id: `efteling_${id}`,
      originalName: name,
      latitude: latlonSplit[0],
      longitude: latlonSplit[1],
    };
  }
}

export default Efteling;
