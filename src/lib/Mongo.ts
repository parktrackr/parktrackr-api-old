/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import { Attraction } from '../mongoose/models/Attraction';
import dotenv from 'dotenv'
dotenv.config()

class Mongo {
  async createAttraction(data: any): Promise<any> {
    const attraction = new Attraction(data);
    await attraction.save();
  }

  async createAttractions(array: any): Promise<any> {
    await Attraction.insertMany(array);
  }
}

export default Mongo;
