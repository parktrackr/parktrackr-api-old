/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */

import fetch from 'node-fetch';
import dotenv from 'dotenv'
dotenv.config()

class WalibiBelgium {
  entertainmentURL: any;
  latitude: number;
  longitude: number;

  constructor() {
    // URL settings
    this.entertainmentURL = process.env.WALIBIBELGIUM_ENTERTAINMENTS_URL;

    this.latitude = 50.70197577556418;
    this.longitude = 4.594111307201126;
  }

  async FetchPOIs(lang: string): Promise<any> {
    return fetch(this.entertainmentURL.replace('LANG', lang), {
      method: 'GET',
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        return data;
      })
      .catch((err) => {
        throw new Error(err);
      });
  }

  ProcessAttractionPOI(data: any): any {
    const { uuid, title, location } = data;
    const { lat, lon } = location;
    return {
      id: `walibibelgium_${uuid}`,
      originalName: title,
      latitude: lat,
      longitude: lon,
    };
  }
}

export default WalibiBelgium;
