import eftelingTests from './efteling/index.test';
import europaParkTest from './europapark/index.test';
import toverlandTests from './toverland/index.test';
import walibiHollandTest from './walibiholland/index.test';
import walibiBelgiumTest from './walibibelgium/index.test';
import walibiRhoneAlpesTest from './walibirhonealpes/index.test';

eftelingTests();
europaParkTest();
toverlandTests();
walibiBelgiumTest();
walibiHollandTest();
walibiRhoneAlpesTest();
