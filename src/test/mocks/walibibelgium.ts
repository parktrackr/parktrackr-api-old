const mockAttraction = {
  uuid: '9870c92c-4f78-4fbb-a16b-f1d3848bd9fe',
  title: 'Octopus',
  image: {
    url: 'https://www.walibi.be/sites/default/files/entertainment/2018-06/IMG_1663.JPG',
    thumbnailUrl:
      'https://www.walibi.be/sites/default/files/styles/400x100pc/public/entertainment/2018-06/IMG_1663.JPG',
  },
  location: { lat: '50.697381131572', lon: '4.5869877934456' },
  status: 'open',
  waitingTime: -10,
  waitingTimeDescription: 'no q-time',
  parameters: [
    { title: 'min accompanied heigh', value: '105 cm' },
    { title: 'min unaccompanied heigh', value: '120 cm' },
  ],
  category: {
    id: 'aa1236c0-162a-4fe3-99cf-1d1924376d14',
    name: 'Familie',
    weight: 63,
    icon: {
      iconType: 'svg',
      imageUrl: 'https://www.walibi.be/themes/custom/walibi_red/html/svg/spritesrc/icon-kids-family.svg',
    },
  },
  additionalContent: [
    {
      title: 'Ten strijde met de Octopus',
      text:
        '<p><strong>Ten strijde met de Octopus</strong></p>\r\n' +
        '\r\n' +
        '<p>De Octopus neemt je in zijn tentakels, slingert je heen en weer, omhoog en omlaag, en draait ontelbare toeren in het rond. Durf jij deze strijd aan?</p>\r\n' +
        '\r\n' +
        '<p><strong>Desoriëntatie</strong></p>\r\n' +
        '\r\n' +
        '<p>Of je je nu verzet of niet, het noorden ben je toch kwijt. Las na deze attractie dus best een pauze in om je oriëntatie terug te vinden!</p>\r\n',
      image: [Object],
    },
  ],
};

export { mockAttraction };
