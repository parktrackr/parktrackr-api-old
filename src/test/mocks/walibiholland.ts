const mockAttraction = {
  uuid: '25f9445f-26d3-4b11-8ca7-c656f79be563',
  title: 'Crazy River',
  image: {
    url: 'https://www.walibi.nl/sites/default/files/entertainment/2020-02/CrazyRiver-YA-01.jpg',
    thumbnailUrl:
      'https://www.walibi.nl/sites/default/files/styles/400x100pc/public/entertainment/2020-02/CrazyRiver-YA-01.jpg',
  },
  location: { lat: '52.441924', lon: '5.764769' },
  status: 'closed',
  waitingTime: -10,
  fastPass: true,
  photoService: true,
  parameters: [
    { title: 'min accompanied heigh', value: '90 cm' },
    { title: 'min unaccompanied heigh', value: '120 cm' },
    { title: 'Baanlengte', value: '524' },
  ],
  category: {
    id: '79a9424f-066e-4ca2-85aa-3be5f7177947',
    name: 'Exciting',
    weight: 52,
    icon: {
      iconType: 'svg',
      imageUrl: 'https://www.walibi.nl/themes/custom/walibi_red/html/svg/spritesrc/icon-thrill-orange.svg',
    },
  },
  additionalContent: [
    {
      title: 'Maak een wilde tocht over het water in een boomstam',
      text:
        '<p>In deze waterachtbaan van maar liefst 524 meter maak je in een boomstam een wilde tocht over het water. Boomstam attractie Crazy River is een must voor elke avonturier die niet bang is om nat te worden!</p>\r\n',
    },
  ],
};

export { mockAttraction };
