const mockAttraction = {
  id: 39,
  code: 550,
  type: 'attraction',
  status: 'live',
  versions: [
    {
      id: 57,
      status: 'live',
      name: 'Pirates in Batavia',
      descriptionMarkup:
        '<p>On <strong>28th July 2020</strong>, the time has finally come: The Pirates in Batavia will re-open their doors! The festive opening will take place in form of a press conference with invited guests. From late afternoon, the attraction will be open for the first guests. From 29th July, the Pirates are looking forward to welcoming many guests daily.</p>\r\n' +
        '\r\n' +
        '<p><strong>Are you ready for a new adventure?</strong></p>\r\n' +
        '\r\n' +
        '<p>Accompany Bartholomeus van Robbemond on his search for the mystical Dagger of Batavia, the Fire Tiger. Legend has it that whoever owns the dagger of Batavia is invulnerable. No wonder then that not only van Robbemond is after the dagger.</p>\r\n' +
        '\r\n' +
        '<p>Prepare yourself for an exciting underground boat ride experience, which closely resembles its original design from 1987. for eight minutes, you will be taken through the exotic harbour city of Batavia - outlandish sounds and unforgettable sceneries, with 100 figures, will surely take your breath away!</p>\r\n' +
        '\r\n' +
        '<p>In case some of the figures seem familiar to you - no wonder! Amongst the many new pirates, monkeys, crocodiles, snakes and more that were made with modern technology, you will also find figures of the original attraction!</p>\r\n' +
        '\r\n' +
        "<p>Would you like to experience some more adventures with Bartholomeus van Robbemond? No problem! In autumn, the first volume of the new novel series on the Adventure Club of Europe will be published by Coppenrath. It will tell you all about van Robbemond's exciting adventures on the seven seas.</p>",
      description:
        'On 28th July 2020, the time has finally come: The Pirates in Batavia will re-open their doors! The festive opening will take place in form of a press conference with invited guests. From late afternoon, the attraction will be open for the first guests. From 29th July, the Pirates are looking forward to welcoming many guests daily.\r\n' +
        '\r\n' +
        'Are you ready for a new adventure?\r\n' +
        '\r\n' +
        'Accompany Bartholomeus van Robbemond on his search for the mystical Dagger of Batavia, the Fire Tiger. Legend has it that whoever owns the dagger of Batavia is invulnerable. No wonder then that not only van Robbemond is after the dagger.\r\n' +
        '\r\n' +
        'Prepare yourself for an exciting underground boat ride experience, which closely resembles its original design from 1987. for eight minutes, you will be taken through the exotic harbour city of Batavia - outlandish sounds and unforgettable sceneries, with 100 figures, will surely take your breath away!\r\n' +
        '\r\n' +
        'In case some of the figures seem familiar to you - no wonder! Amongst the many new pirates, monkeys, crocodiles, snakes and more that were made with modern technology, you will also find figures of the original attraction!\r\n' +
        '\r\n' +
        "Would you like to experience some more adventures with Bartholomeus van Robbemond? No problem! In autumn, the first volume of the new novel series on the Adventure Club of Europe will be published by Coppenrath. It will tell you all about van Robbemond's exciting adventures on the seven seas.",
      excerpt: 'Attention all landlubbers! Pirates are in sight!',
      image: [Object],
      icon: [Object],
      iconSvg: null,
      youtube: null,
      galleryMedias: [Array],
      changedAt: '2020-07-23T16:47:51+02:00',
      startAt: null,
      endAt: null,
      validFrom: '2020-07-21T11:28:42+02:00',
      validTo: '2020-07-28T14:00:00+02:00',
    },
  ],
  latitude: 48.26357079,
  longitude: 7.72040079,
  name: 'Pirates in Batavia',
  descriptionMarkup:
    '<p>Accompany Bartholomeus van Robbemond on his search for the mystical Dagger of Batavia, the Fire Tiger. Legend has it that whoever owns the dagger of Batavia is invulnerable. No wonder then that not only van Robbemond is after the dagger.</p>\r\n' +
    '\r\n' +
    '<p>Prepare yourself for an exciting underground boat ride experience, which closely resembles its original design from 1987. for eight minutes, you will be taken through the exotic harbour city of Batavia - outlandish sounds and unforgettable sceneries, with 100 figures, will surely take your breath away!</p>',
  description:
    'Accompany Bartholomeus van Robbemond on his search for the mystical Dagger of Batavia, the Fire Tiger. Legend has it that whoever owns the dagger of Batavia is invulnerable. No wonder then that not only van Robbemond is after the dagger.\r\n' +
    '\r\n' +
    'Prepare yourself for an exciting underground boat ride experience, which closely resembles its original design from 1987. for eight minutes, you will be taken through the exotic harbour city of Batavia - outlandish sounds and unforgettable sceneries, with 100 figures, will surely take your breath away!',
  excerpt: 'Pirates are in sight!',
  mainSponsors: [],
  sideSponsors: [],
  poiAttributes: [
    {
      id: 782,
      ident: 'opening',
      icon: [Object],
      label: 'Opening',
      value: '2020',
      text: '2020',
    },
    {
      id: 783,
      ident: 'driving_time',
      icon: [Object],
      label: 'Driving Time',
      value: '8:00',
      text: '8:00 min.',
    },
    {
      id: 784,
      ident: 'capacity_boat',
      icon: [Object],
      label: 'Capacity',
      value: '16',
      text: '16 persons per boat',
    },
    {
      id: 785,
      ident: 'theoretical_capacity_hour',
      icon: [Object],
      label: 'Theoretical Capacity',
      value: '1800',
      text: '1800 persons per hour',
    },
    {
      id: 786,
      ident: 'producer',
      icon: [Object],
      label: 'Producer',
      value: 'MACK Rides',
      text: 'MACK Rides',
    },
  ],
  attributes: [{ key: 'Opening', value: '2020' }],
  poiActions: [],
  actions: [],
  image: {
    id: 5555,
    name: 'PiratenInBatavia_Holland_Europa-Park.jpg',
    autor: null,
    alt: 'Attraktion Piraten in Batavia im Themenbereich Holland',
    copyright: null,
    mime: 'image/jpeg',
    changedAt: '2020-08-17T15:16:44+02:00',
    reference:
      'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/76195e235546d89725a58de25e7c49a1b2d901df.jpeg',
    provider: '76195e235546d89725a58de25e7c49a1b2d901df.jpeg',
    small: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5555_default_small.jpeg',
    medium: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5555_default_medium.jpeg',
    large: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5555_default_large.jpeg',
    xlarge: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5555_default_xlarge.jpeg',
    '480p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5555_default_480p.jpeg',
    '720p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5555_default_720p.jpeg',
    '1080p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5555_default_1080p.jpeg',
    '48x48': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5555_default_48x48.jpeg',
    '96x96': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5555_default_96x96.jpeg',
    '144x144': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5555_default_144x144.jpeg',
  },
  icon: {
    id: 1699,
    name: 'ico_attraktionen_kapazitaet01_themenfahrt.png',
    autor: null,
    alt: null,
    copyright: null,
    mime: 'image/png',
    changedAt: '2019-11-06T12:30:48+01:00',
    reference:
      'https://storage.googleapis.com/epcloud-ots-prod/default/0001/02/a8a101c2fb4fe1e32c14ec274adc600c4d003695.png',
    provider: 'a8a101c2fb4fe1e32c14ec274adc600c4d003695.png',
    small: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/02/thumb_1699_default_small.png',
    medium: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/02/thumb_1699_default_medium.png',
    large: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/02/thumb_1699_default_large.png',
    xlarge: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/02/thumb_1699_default_xlarge.png',
    '480p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/02/thumb_1699_default_480p.png',
    '720p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/02/thumb_1699_default_720p.png',
    '1080p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/02/thumb_1699_default_1080p.png',
    '48x48': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/02/thumb_1699_default_48x48.png',
    '96x96': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/02/thumb_1699_default_96x96.png',
    '144x144': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/02/thumb_1699_default_144x144.png',
  },
  iconSvg: null,
  youtube: {
    id: 2505,
    name: '1st Teaser "Piraten in Batavia" 2020 im Europa-Park',
    autor: 'Europa-Park',
    alt: null,
    copyright: null,
    mime: 'video/x-flv',
    changedAt: '2019-11-19T15:07:36+01:00',
    reference: 'https://www.youtube.com/watch?v=f5u7N6ivGZo',
    provider: 'f5u7N6ivGZo',
    small: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/03/thumb_2505_default_small.jpg',
    medium: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/03/thumb_2505_default_medium.jpg',
    large: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/03/thumb_2505_default_large.jpg',
    xlarge: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/03/thumb_2505_default_xlarge.jpg',
    '480p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/03/thumb_2505_default_480p.jpg',
    '720p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/03/thumb_2505_default_720p.jpg',
    '1080p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/03/thumb_2505_default_1080p.jpg',
    '48x48': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/03/thumb_2505_default_48x48.jpg',
    '96x96': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/03/thumb_2505_default_96x96.jpg',
    '144x144': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/03/thumb_2505_default_144x144.jpg',
  },
  galleryMedias: [
    {
      id: 5150,
      name: 'GAL_Batavia_neu_1.jpg',
      autor: null,
      alt: null,
      copyright: null,
      mime: 'image/jpeg',
      changedAt: '2020-07-30T11:51:37+02:00',
      reference:
        'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/39a6775f119608ee62ba7c5ca00039ee450f8ba0.jpeg',
      provider: '39a6775f119608ee62ba7c5ca00039ee450f8ba0.jpeg',
      small: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5150_default_small.jpeg',
      medium: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5150_default_medium.jpeg',
      large: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5150_default_large.jpeg',
      xlarge: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5150_default_xlarge.jpeg',
      '480p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5150_default_480p.jpeg',
      '720p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5150_default_720p.jpeg',
      '1080p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5150_default_1080p.jpeg',
      '48x48': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5150_default_48x48.jpeg',
      '96x96': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5150_default_96x96.jpeg',
      '144x144': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5150_default_144x144.jpeg',
    },
    {
      id: 5151,
      name: 'GAL_Batavia_neu_2.jpg',
      autor: null,
      alt: null,
      copyright: null,
      mime: 'image/jpeg',
      changedAt: '2020-07-30T11:52:06+02:00',
      reference:
        'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/cd45d987b1416e46b3e820ba5c763195d0bba6f9.jpeg',
      provider: 'cd45d987b1416e46b3e820ba5c763195d0bba6f9.jpeg',
      small: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5151_default_small.jpeg',
      medium: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5151_default_medium.jpeg',
      large: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5151_default_large.jpeg',
      xlarge: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5151_default_xlarge.jpeg',
      '480p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5151_default_480p.jpeg',
      '720p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5151_default_720p.jpeg',
      '1080p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5151_default_1080p.jpeg',
      '48x48': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5151_default_48x48.jpeg',
      '96x96': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5151_default_96x96.jpeg',
      '144x144': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5151_default_144x144.jpeg',
    },
    {
      id: 5152,
      name: 'GAL_Batavia_neu_3.jpg',
      autor: null,
      alt: null,
      copyright: null,
      mime: 'image/jpeg',
      changedAt: '2020-07-30T11:52:18+02:00',
      reference:
        'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/dc0e93a0d075e5bf7beb2f65f099d6702ff57471.jpeg',
      provider: 'dc0e93a0d075e5bf7beb2f65f099d6702ff57471.jpeg',
      small: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5152_default_small.jpeg',
      medium: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5152_default_medium.jpeg',
      large: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5152_default_large.jpeg',
      xlarge: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5152_default_xlarge.jpeg',
      '480p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5152_default_480p.jpeg',
      '720p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5152_default_720p.jpeg',
      '1080p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5152_default_1080p.jpeg',
      '48x48': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5152_default_48x48.jpeg',
      '96x96': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5152_default_96x96.jpeg',
      '144x144': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5152_default_144x144.jpeg',
    },
    {
      id: 5153,
      name: 'GAL_Batavia_neu_4.jpg',
      autor: null,
      alt: null,
      copyright: null,
      mime: 'image/jpeg',
      changedAt: '2020-07-30T11:52:38+02:00',
      reference:
        'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/c94f5f7b818bdfe79c834bca71c5abb460f4deb9.jpeg',
      provider: 'c94f5f7b818bdfe79c834bca71c5abb460f4deb9.jpeg',
      small: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5153_default_small.jpeg',
      medium: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5153_default_medium.jpeg',
      large: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5153_default_large.jpeg',
      xlarge: 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5153_default_xlarge.jpeg',
      '480p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5153_default_480p.jpeg',
      '720p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5153_default_720p.jpeg',
      '1080p': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5153_default_1080p.jpeg',
      '48x48': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5153_default_48x48.jpeg',
      '96x96': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5153_default_96x96.jpeg',
      '144x144': 'https://storage.googleapis.com/epcloud-ots-prod/default/0001/06/thumb_5153_default_144x144.jpeg',
    },
  ],
  relatedIds: [],
  openTimes: [],
  openTimeDescription: null,
  seasonRelated: false,
  startAt: null,
  endAt: null,
  validFrom: null,
  validTo: null,
  changedAt: '2020-11-12T15:09:11+01:00',
  minAge: null,
  minAgeAdult: null,
  maxAge: 0,
  minHeight: null,
  minHeightAdult: null,
  maxHeight: null,
  scopes: ['europapark'],
  areaId: 17,
  filterIds: [27, 39, 41],
  benefitIds: [],
  shows: [],
  highlight: false,
  superior: false,
  stars: null,
  queueing: false,
  queueingStopped: false,
  queueingStoppedAt: null,
  slotCapacity: 0,
  slotLength: 0,
  slotBuffer: 0,
  queueingMaxWaitingTime: 0,
  queueingMaxWaitingTimeClosed: 0,
  infostele: false,
  closingCode: null,
  typeCode: null,
};

export { mockAttraction };
