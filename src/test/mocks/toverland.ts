const mockAttraction = {
  id: 16,
  name: "Maximus' Blitz Bahn",
  area_id: '2',
  latitude: '51.397333',
  longitude: '5.986306',
  short_description: {
    nl: "Word Maximus' snelste testrijder!",
    de: "Werde Maximus' schnellster Testfahrer",
    en: "Become Maximus' fastest test driver!",
  },
  description: {
    nl:
      'Jij bent één van de uitverkorenen die de Bobslee van Maximus Müller mag uitproberen. Neem plaats, scheer door de bochten en zet recordtijd neer waarmee je je gezelschap verslaat.',
    de:
      'Du bist auserwählt, um die Bobbahn von Maximus Müller zu testen. Nimm Platz, rase durch die Kurvern und breche die Rekordzeit, mit der du alle verwundern wirst.',
    en: 'Maximus’ Blitz Bahn is a bobsleigh run following the latest technologies.  Je bepaalt zelf hoe hard je gaat!',
  },
  thumbnail:
    'https://www.toverland.com/fileadmin/user_upload/App/Attractie/Maximus_Blitz_Bahn/Blitzbahn_moeder_dochter_winter_close-up.jpg',
  minLength: '9',
  supervision: '34',
  header_description: { nl: 'Bobbaan', de: 'Bobbahn', en: 'Bobsleigh track' },
};

export { mockAttraction };
