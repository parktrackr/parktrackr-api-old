const mockAttraction = {
  uuid: '4acf5d16-c168-4202-98fb-cefd7461c821',
  title: 'Timber',
  image: {
    url: 'https://www.walibi.fr/sites/default/files/entertainment/2019-10/wra-timber.jpg',
    thumbnailUrl:
      'https://www.walibi.fr/sites/default/files/styles/400x100pc/public/entertainment/2019-10/wra-timber.jpg',
  },
  location: { lat: '45.622874900432', lon: '5.5672798678279' },
  status: 'closed',
  waitingTime: -10,
  fastPass: true,
  babySwitch: true,
  photoService: true,
  parameters: [
    { title: 'min accompanied heigh', value: '110 cm' },
    { title: 'min unaccompanied heigh', value: '120 cm' },
  ],
  category: {
    id: '757e0a67-f61b-4b93-a6f3-66525bbf7715',
    name: 'Thrills',
    weight: 91,
    icon: {
      iconType: 'svg',
      imageUrl: 'https://www.walibi.fr/themes/custom/walibi_red/html/svg/spritesrc/icon-ent-type-frissons.svg',
    },
  },
  additionalContent: [
    {
      title: 'The Russian Mountains that could set the world on fire!',
      text:
        '<p><strong>The Russian Mountains that are on fire!&nbsp;</strong></p>\r\n' +
        '\r\n' +
        "<p>Climb aboard <strong>this new 2016 ride</strong> and take your place in a state of the art machine where you will get to go chop wood at speed! After an initial hair-raising drop, there'll be air times and bends of up to 59° - <strong>a thrill for the whole family!</strong></p>\r\n",
    },
  ],
};

export { mockAttraction };
