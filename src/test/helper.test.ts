import mongoose from 'mongoose';

before((done) => {
  mongoose.connect('mongodb://localhost/parktrackr-api-test', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  });
  mongoose.connection
    .once('open', () => {
      done();
    })
    .on('error', (error) => console.warn('Warning', error));
});

beforeEach(async () => {
  const { attractions } = mongoose.connection.collections;
  const collections = await mongoose.connection.db.listCollections().toArray();
  if (collections.some((col) => col.name === 'attractions')) {
    await attractions.drop();
  }
  return;
});
