/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import Toverland from '../../lib/Toverland/Toverland';
import { mockAttraction } from '../mocks/toverland';

const processTests = (): any =>
  describe('Process', () => {
    const toverland = new Toverland();

    it('processes poi to Attraction model data', async () => {
      const processedData = toverland.ProcessAttractionPOI(mockAttraction);
      assert(processedData.id === 'toverland_16');
      assert(processedData.originalName === 'Maximus\' Blitz Bahn');
      assert(processedData.latitude === '51.397333');
      assert(processedData.longitude === '5.986306');
    });
  });

export default processTests;
