/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import Toverland from '../../lib/Toverland/Toverland';

const fetchTests = (): any =>
  describe('Fetch', () => {
    const toverland = new Toverland();

    const assertPOI = (poi: any) => {
      const { id, name, latitude, longitude } = poi;
      assert(typeof id === 'number');
      assert(typeof name === 'string');

      assert(typeof latitude === 'string');
      assert(typeof longitude === 'string');

      assert(latitude > 51.3 && latitude < 51.4);
      assert(longitude > 5.9 && longitude < 6);

      return true;
    };

    it('Fetches POIs', async () => {
      const data = await toverland.FetchRidePOIs();
      assert(data.every((poi: any) => assertPOI(poi)));
    });
  });

export default fetchTests;
