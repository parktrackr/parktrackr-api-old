/* eslint-disable @typescript-eslint/no-explicit-any */
import fetchTests from './fetch.test';
import processTests from './process.test';

const toverlandTests = (): any =>
  describe('Toverland', () => {
    fetchTests();
    processTests();
  });

export default toverlandTests;
