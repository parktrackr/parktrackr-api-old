/* eslint-disable @typescript-eslint/no-explicit-any */
import createTests from './create.test';
import readTests from './read.test';
import updateTests from './update.test';
import deleteTests from './delete.test';
import validationTests from './validation.test';

const generalTests = (): any =>
  describe('General', () => {
    createTests();
    readTests();
    updateTests();
    deleteTests();
    validationTests();
  });

export default generalTests;
