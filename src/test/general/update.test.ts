/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import models from '../../mongoose/models';

const { Attraction } = models;

const updateTests = (): any =>
  describe('Update', () => {
    let mockProcessedAttraction: any;

    beforeEach(async () => {
      mockProcessedAttraction = new Attraction({
        id: 'efteling_baron1898',
        originalName: 'Baron 1898',
        latitude: '10.00000',
        longitude: '10.00000',
      });
      await mockProcessedAttraction.save();
      return;
    });

    it('instance type using set and save', async () => {
      mockProcessedAttraction.set('originalName', 'Baron 2020');

      await mockProcessedAttraction.save();

      const attractions = await Attraction.find({});
      assert(attractions.length === 1);
      assert(attractions[0].originalName === 'Baron 2020');

      return;
    });

    it('a model instance can update', async () => {
      await mockProcessedAttraction.updateOne({
        originalName: 'Baron 2020',
      });

      const attractions = await Attraction.find({});

      assert(attractions.length === 1);
      assert(attractions[0].originalName === 'Baron 2020');

      return;
    });

    it('a model class can update', async () => {
      await Attraction.updateMany({ originalName: 'Baron 1898' }, { originalName: 'Baron 2020' });

      const attractions = await Attraction.find({});

      assert(attractions.length === 1);
      assert(attractions[0].originalName === 'Baron 2020');

      return;
    });

    it('a model class can update one record', async () => {
      await Attraction.findOneAndUpdate({ originalName: 'Baron 1898' }, { originalName: 'Baron 2020' });

      const attractions = await Attraction.find({});
      assert(attractions.length === 1);
      assert(attractions[0].originalName === 'Baron 2020');

      return;
    });

    it('a model class can find a record with an Id and update', async () => {
      await Attraction.findByIdAndUpdate(mockProcessedAttraction._id, { originalName: 'Baron 2020' });

      const attractions = await Attraction.find({});

      assert(attractions.length === 1);
      assert(attractions[0].originalName === 'Baron 2020');

      return;
    });
  });

export default updateTests;
