/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import models from '../../mongoose/models';

const { Attraction } = models;

const deleteTests = (): any =>
  describe('Delete', () => {
    let mockProcessedAttraction: any;

    beforeEach(async () => {
      mockProcessedAttraction = new Attraction({
        id: 'efteling_baron1898',
        originalName: 'Baron 1898',
        latitude: '10.00000',
        longitude: '10.00000',
      });
      await mockProcessedAttraction.save();
    });

    it('model instance remove', async () => {
      await mockProcessedAttraction.remove();
      const removedUser = await Attraction.findOne({ name: 'Joe' });
      assert(removedUser === null);
      return;
    });

    it('class method remove', async () => {
      await Attraction.deleteMany({ id: 'efteling_baron1898' });
      const removedUser = await Attraction.find({ name: 'Joe' });
      assert(removedUser.length === 0);
      return;
    });

    it('class method find and remove', async () => {
      await Attraction.findOneAndRemove({ id: 'efteling_baron1898' });
      const removedUser = await Attraction.findOne({ name: 'Joe' });
      assert(removedUser === null);
      return;
    });

    it('class method find by id and remove', async () => {
      await Attraction.findByIdAndRemove(mockProcessedAttraction._id);
      const removedUser = await Attraction.findOne({ name: 'Joe' });
      assert(removedUser === null);
      return;
    });
  });

export default deleteTests;
