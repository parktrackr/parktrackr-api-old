/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import models from '../../mongoose/models';

const { Attraction } = models;

const createTests = (): any =>
  context('Create', () => {
    const mockProcessedAttraction = {
      id: 'efteling_avonturendoolhof',
      originalName: 'Avonturendooholf',
      latitude: '10.00000',
      longitude: '10.00000',
    };

    const mockProcessedAttractionTwo = {
      id: 'efteling_baron1898',
      originalName: 'Baron 1898',
      latitude: '10.00000',
      longitude: '10.00000',
    };

    const mockProcessedAttractionThree = {
      id: 'efteling_python',
      originalName: 'Python',
      latitude: '10.00000',
      longitude: '10.00000',
    };

    it('Creates Dutch Attraction POI', async () => {
      const attractionDoc = new Attraction(mockProcessedAttraction);
      await attractionDoc.save();
      assert(!attractionDoc.isNew, 'TestAttraction model is still new.');
    });

    it('Creates multiple Attraction POIs', async () => {
      const attractions = [mockProcessedAttraction, mockProcessedAttractionTwo, mockProcessedAttractionThree];
      await Attraction.insertMany(attractions);
      const foundData = await Attraction.find({});
      const currentTime = new Date();

      assert(foundData.length === attractions.length, 'Not all data is saved.');

      // Property Data
      assert(
        foundData.every((data: any) => typeof data.id === 'string'),
        'id is not a String.',
      );
      assert(
        foundData.every((data, index) => data.id === attractions[index].id),
        'id does not match id from data.',
      );
      assert(
        foundData.every((data: any) => typeof data.originalName === 'string'),
        'originalName is not a String.',
      );
      assert(
        foundData.every((data: any, index) => data.originalName === attractions[index].originalName),
        'originalName does not match id from data.',
      );

      // Mongoose Data
      assert(
        foundData.every((data: any) => data.__v === 0),
        'Entry has been updated.',
      );
      assert(
        foundData.every((data: any) => new Date(data.createdAt).setHours(0, 0, 0, 0) === currentTime.setHours(0, 0, 0, 0)),
        'createdAt is not today.',
      );
      assert(
        foundData.every((data: any) => new Date(data.updatedAt).setHours(0, 0, 0, 0) === currentTime.setHours(0, 0, 0, 0)),
        'createdAt is not today.',
      );
      assert(
        foundData.every(
          (data: any) => new Date(data.createdAt).setHours(0, 0, 0, 0) === new Date(data.updatedAt).setHours(0, 0, 0, 0),
        ),
        'createdAt and updatedAt are not the same.',
      );
    });
  });

export default createTests;
