/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import models from '../../mongoose/models';

const { Attraction } = models;

const validationTests = (): any =>
  describe('Validation', () => {
    it('requires an originalName', () => {
      const attraction = new Attraction({ originalName: undefined });

      const validationResult = attraction.validateSync();
      if (validationResult) {
        const { message } = validationResult.errors.originalName;
        assert(message === 'originalName is required.', 'Error message does not match.');
      }
      
    });

    it('requires an id', () => {
      const attraction = new Attraction({ id: undefined });

      const validationResult = attraction.validateSync();
      if (validationResult) {
        const { message } = validationResult.errors.id;
        assert(message === 'id is required.', 'Error message does not match.');
      }
      
    });

    it('requires a latitude', () => {
      const attraction = new Attraction({ latitude: undefined });

      const validationResult = attraction.validateSync();
      if (validationResult) {
        const { message } = validationResult.errors.latitude;
        assert(message === 'latitude is required.', 'Error message does not match.');
      }
      
    });

    it('requires a longitude', () => {
      const attraction = new Attraction({ longitude: undefined });

      const validationResult = attraction.validateSync();
      if (validationResult) {
        const { message } = validationResult.errors.longitude;
        assert(message === 'longitude is required.', 'Error message does not match.');
      }
      
    });

    it('requires id to be build from parkId and attractionId', () => {
      const attraction = new Attraction({ id: 'baron1898', originalName: 'Baron 1898' });

      const validationResult = attraction.validateSync();
      if (validationResult) {
        const { message } = validationResult.errors.id;
        assert(
          message === 'id can only have letters, numbers, - and _., and should be ParkID_AttractionID.',
          'Error message does not match.',
        );
      }
      
    });

    it('latitude checks input', () => {
      const attraction = new Attraction({ latitude: '44533.0355' });

      const validationResult = attraction.validateSync();
      if (validationResult) {
        const { message } = validationResult.errors.latitude;
        assert(message === 'latitude should exist of (XX)X.XXXXX.', 'Error message does not match');
      }
      
    });

    it('longitude checks input', () => {
      const attraction = new Attraction({ longitude: '44533.0355' });

      const validationResult = attraction.validateSync();
      if (validationResult) {
        const { message } = validationResult.errors.longitude;
        assert(message === 'longitude should exist of (XX)X.XXXXX.', 'Error message does not match');
      }
      
    });
  });

export default validationTests;
