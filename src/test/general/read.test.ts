/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import models from '../../mongoose/models';

const { Attraction } = models;

const readTests = (): any =>
  context('Read', () => {
    let mockProcessedAttraction: any, mockProcessedAttractionTwo: any, mockProcessedAttractionThree: any, mockProcessedAttractionFour: any;

    beforeEach(async () => {
      mockProcessedAttraction = new Attraction({
        id: 'efteling_avonturendoolhof',
        originalName: 'Avonturendooholf',
        latitude: '10.00000',
        longitude: '10.00000',
      });

      mockProcessedAttractionTwo = new Attraction({
        id: 'efteling_baron1898',
        originalName: 'Baron 1898',
        latitude: '10.00000',
        longitude: '10.00000',
      });

      mockProcessedAttractionThree = new Attraction({
        id: 'efteling_python',
        originalName: 'Python',
        latitude: '10.00000',
        longitude: '10.00000',
      });

      mockProcessedAttractionFour = new Attraction({
        id: 'efteling_symbolica',
        originalName: 'Symbolica',
        latitude: '10.00000',
        longitude: '10.00000',
      });

      await mockProcessedAttraction.save();
      await mockProcessedAttractionTwo.save();
      await mockProcessedAttractionThree.save();
      await mockProcessedAttractionFour.save();
      return;
    });

    it('finds all attractions with the originalName of Python', async () => {
      const attractions = await Attraction.find({ originalName: 'Python' });
      assert(attractions[0]._id.toString() === mockProcessedAttractionThree._id.toString());
      return;
    });

    it('finds specific attraction with the _id of Python', async () => {
      const attraction = await Attraction.findOne({ _id: mockProcessedAttractionThree._id });
      if (attraction !== null) {
        assert(attraction.originalName === 'Python');
      }
      return;
    });

    it('can skip and limit the result set', async () => {
      const attractions = await Attraction.find({}).sort({ originalName: 1 }).skip(1).limit(2);
      assert(attractions.length === 2);
      assert(attractions[0].originalName === 'Baron 1898');
      assert(attractions[1].originalName === 'Python');
      return;
    });
  });

export default readTests;
