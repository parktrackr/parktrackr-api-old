/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import WalibiBelgium from '../../lib/WalibiBelgium/WalibiBelgium';
import { mockAttraction } from '../mocks/walibibelgium';

const processTests = (): any =>
  describe('Process', () => {
    const walibiBelgium = new WalibiBelgium();

    it('processes poi to Attraction model data', async () => {
      const processedData = walibiBelgium.ProcessAttractionPOI(mockAttraction);
      assert(processedData.id === 'walibibelgium_9870c92c-4f78-4fbb-a16b-f1d3848bd9fe');
      assert(processedData.originalName === 'Octopus');
      assert(processedData.latitude === '50.697381131572');
      assert(processedData.longitude === '4.5869877934456');
    });
  });

export default processTests;
