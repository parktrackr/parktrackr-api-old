/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import WalibiBelgium from '../../lib/WalibiBelgium/WalibiBelgium';

const fetchTests = (): any =>
  describe('Fetch', () => {
    const walibiBelgium = new WalibiBelgium();

    const assertPOI = (poi: any) => {
      const { uuid, title, location } = poi;
      const { lat, lon } = location;

      assert(typeof uuid === 'string');
      assert(typeof title === 'string');

      assert(typeof lat === 'string');
      assert(typeof lon === 'string');

      assert(lat > 50.6 && lat < 50.8);
      assert(lon > 4.5 && lon < 4.6);

      return true;
    };

    it('Fetches Dutch POIs', async () => {
      const data = await walibiBelgium.FetchPOIs('nl');
      const attractions = data.entertainment;
      assert(attractions.every((poi: any) => assertPOI(poi)));
    });
    it('Fetches English POIs', async () => {
      const data = await walibiBelgium.FetchPOIs('en');
      const attractions = data.entertainment;
      assert(attractions.every((poi: any) => assertPOI(poi)));
    });
    it('Fetches French POIs', async () => {
      const data = await walibiBelgium.FetchPOIs('fr');
      const attractions = data.entertainment;
      assert(attractions.every((poi: any) => assertPOI(poi)));
    });
  });

export default fetchTests;
