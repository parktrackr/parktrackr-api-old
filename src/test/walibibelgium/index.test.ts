/* eslint-disable @typescript-eslint/no-explicit-any */
import fetchTests from './fetch.test';
import processTests from './process.test';

const walibiBelgiumTest = (): any =>
  describe('Walibi Belgium', () => {
    fetchTests();
    processTests();
  });

export default walibiBelgiumTest;
