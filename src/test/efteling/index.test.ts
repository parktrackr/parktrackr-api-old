/* eslint-disable @typescript-eslint/no-explicit-any */
import fetchTests from './fetch.test';
import processTests from './process.test';

const eftelingTests = (): any =>
  describe('Efteling', () => {
    fetchTests();
    processTests();
  });

export default eftelingTests;
