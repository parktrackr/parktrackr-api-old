/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import Efteling from '../../lib/Efteling/Efteling';
import { mockAttraction } from '../mocks/efteling';

const processTests = (): any =>
  describe('Process', () => {
    const efteling = new Efteling();

    it('processes hit to Attraction model data', async () => {
      const processedData = efteling.ProcessAttractionPOI(mockAttraction);

      assert(processedData.id === 'efteling_baron1898');
      assert(processedData.originalName === 'Baron 1898');
      assert(processedData.latitude === '51.64827');
      assert(processedData.longitude === '5.050988');
    });
  });

export default processTests;
