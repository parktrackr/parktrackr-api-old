/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import Efteling from '../../lib/Efteling/Efteling';

const fetchTests = (): any =>
  describe('Fetch', () => {
    const efteling = new Efteling();

    const assertPOIData = (data: any) => {
      const { status, hits } = data;
      const { rid, 'time-ms': timeMs } = status;
      const { found, start, hit } = hits;

      assert(typeof rid === 'string', 'rid is not a String.');

      assert(typeof timeMs === 'number', 'time-ms is not a Number.');

      assert(typeof found === 'number', 'found is not a Number.');
      assert(found > 0, 'found is not higher than zero.');

      assert(typeof start === 'number', 'start is not a Number.');
      assert(start === 0, 'start is higher than zero.');

      assert(Array.isArray(hit), 'hit is not an Array.');
      assert(hit.length > 0, 'hit length is zero.');

      return true;
    };

    const assertPOI = (poi: any) => {
      const { id, fields } = poi;
      const { name, language, latlon } = fields;
      const [latitude, longitude] = poi.fields.latlon.split(',');

      assert(typeof id === 'string', 'id is not a String.');
      assert(id.includes(`-${language}`), 'id does not specify language.');

      assert(typeof fields.id === 'string', 'field id is not a String.');
      assert(id === `${fields.id}-${language}`, 'field id does not match id.');

      assert(typeof name === 'string', 'name is not a String.');

      assert(typeof latlon === 'string', 'latlon is not a String.');

      assert(latitude === '0.0' || (latitude > 51.6 && latitude < 51.7));
      assert(longitude === '0.0' || (longitude > 5 && longitude < 5.1));

      return true;
    };

    it('Fetches Dutch POIs', async () => {
      const data = await efteling.FetchPOIs('nl');
      assert(assertPOIData(data));
      assert(data.hits.hit.every((poi: any) => assertPOI(poi)));
    });

    it('Fetches English POIs', async () => {
      const data = await efteling.FetchPOIs('en');
      assert(assertPOIData(data));
      assert(data.hits.hit.every((poi: any) => assertPOI(poi)));
    });

    it('Fetches German POIs', async () => {
      const data = await efteling.FetchPOIs('de');
      assert(assertPOIData(data));
      assert(data.hits.hit.every((poi: any) => assertPOI(poi)));
    });

    it('Fetches French POIs', async function () {
      const data = await efteling.FetchPOIs('fr');
      assert(assertPOIData(data));
      assert(data.hits.hit.every((poi: any) => assertPOI(poi)));
    });
  });

export default fetchTests;
