/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import WalibiRhoneAlpes from '../../lib/WalibiRhoneAlpes/WalibiRhoneAlpes';
import { mockAttraction } from '../mocks/walibirhonealpes';

const processTests = (): any =>
  describe('Process', () => {
    const walibiRhoneAlpes = new WalibiRhoneAlpes();

    it('processes poi to Attraction model data', async () => {
      const processedData = walibiRhoneAlpes.ProcessAttractionPOI(mockAttraction);
      assert(processedData.id === 'walibirhonealpes_4acf5d16-c168-4202-98fb-cefd7461c821');
      assert(processedData.originalName === 'Timber');
      assert(processedData.latitude === '45.622874900432');
      assert(processedData.longitude === '5.5672798678279');
    });
  });

export default processTests;
