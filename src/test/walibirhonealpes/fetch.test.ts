/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import WalibiRhoneAlpes from '../../lib/WalibiRhoneAlpes/WalibiRhoneAlpes';

const fetchTests = (): any =>
  describe('Fetch', () => {
    const walibiRhoneAlpes = new WalibiRhoneAlpes();

    const assertPOI = (poi: any) => {
      const { uuid, title, location } = poi;
      const { lat, lon } = location;

      assert(typeof uuid === 'string');
      assert(typeof title === 'string');

      assert(typeof lat === 'string');
      assert(typeof lon === 'string');

      assert(lat > 45.6 && lat < 45.7);
      assert(lon > 5.5 && lon < 5.6);

      return true;
    };

    it('Fetches English POIs', async () => {
      const data = await walibiRhoneAlpes.FetchPOIs('en');
      const attractions = data.entertainment;
      assert(attractions.every((poi: any) => assertPOI(poi)));
    });
    it('Fetches French POIs', async () => {
      const data = await walibiRhoneAlpes.FetchPOIs('fr');
      const attractions = data.entertainment;
      assert(attractions.every((poi: any) => assertPOI(poi)));
    });
  });

export default fetchTests;
