/* eslint-disable @typescript-eslint/no-explicit-any */

import fetchTests from './fetch.test';
import processTests from './process.test';

const walibiRhoneAlpesTest = (): any =>
  describe('Walibi Rhône-Alpes', () => {
    fetchTests();
    processTests();
  });

export default walibiRhoneAlpesTest;
