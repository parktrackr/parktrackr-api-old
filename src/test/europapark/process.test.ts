/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import EuropaPark from '../../lib/EuropaPark/EuropaPark';
import { mockAttraction } from '../mocks/europapark';

const processTests = (): any =>
  describe('Process', () => {
    const europaPark = new EuropaPark();

    it('processes poi to Attraction model data', async () => {
      const processedData = europaPark.ProcessAttractionPOI(mockAttraction);
      assert(processedData.id === 'europapark_39');
      assert(processedData.originalName === 'Pirates in Batavia');
      assert(processedData.latitude === '48.26357079');
      assert(processedData.longitude === '7.72040079');
    });
  });

export default processTests;
