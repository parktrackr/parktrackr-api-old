/* eslint-disable @typescript-eslint/no-explicit-any */
import fetchTests from './fetch.test';
import processTests from './process.test';

const europaParkTest = (): any =>
  describe('Europa-Park', () => {
    fetchTests();
    processTests();
  });

export default europaParkTest;
