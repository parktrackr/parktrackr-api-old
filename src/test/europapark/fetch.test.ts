/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-unused-vars */
import { assert } from 'chai';
import EuropaPark from '../../lib/EuropaPark/EuropaPark';

const fetchTests = (): any =>
  describe('Fetch', () => {
    const europaPark = new EuropaPark();

    const assertPOI = (poi: any) => {
      const { id, name, latitude, longitude } = poi;

      assert(typeof id === 'number');
      assert(typeof name === 'string');

      assert(typeof latitude === 'number');
      assert(typeof longitude === 'number');

      assert(latitude > 48.2 && latitude < 48.3);
      assert(longitude > 7.7 && longitude < 7.8);
      return true;
    };

    it('Fetches POIs', async () => {
      const data = await europaPark.FetchPOIs();
      const { pois } = data;
      const attractions = pois.filter((poi: any) => poi.type === 'attraction');
      assert(attractions.every((poi: any) => assertPOI(poi)));
    });
  });

export default fetchTests;
