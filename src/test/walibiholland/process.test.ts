/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import WalibiHolland from '../../lib/WalibiHolland/WalibiHolland';
import { mockAttraction } from '../mocks/walibiholland';

const processTests = (): any =>
  describe('Process', () => {
    const walibiHolland = new WalibiHolland();

    it('processes poi to Attraction model data', async () => {
      const processedData = walibiHolland.ProcessAttractionPOI(mockAttraction);
      assert(processedData.id === 'walibiholland_25f9445f-26d3-4b11-8ca7-c656f79be563');
      assert(processedData.originalName === 'Crazy River');
      assert(processedData.latitude === '52.441924');
      assert(processedData.longitude === '5.764769');
    });
  });

export default processTests;
