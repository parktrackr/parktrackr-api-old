/* eslint-disable @typescript-eslint/no-explicit-any */
import { assert } from 'chai';
import WalibiHolland from '../../lib/WalibiHolland/WalibiHolland';

const fetchTests = (): any =>
  describe('Fetch', () => {
    const walibiHolland = new WalibiHolland();

    const assertPOI = (poi: any) => {
      const { uuid, title, location } = poi;
      const { lat, lon } = location;

      assert(typeof uuid === 'string');
      assert(typeof title === 'string');

      assert(typeof lat === 'string');
      assert(typeof lon === 'string');

      assert(lat > 52.4 && lat < 52.5);
      assert(lon > 5.7 && lon < 5.8);

      return true;
    };

    it('Fetches Dutch POIs', async () => {
      const data = await walibiHolland.FetchPOIs('nl');
      const attractions = data.entertainment;
      assert(attractions.every((poi: any) => assertPOI(poi)));
    });
    it('Fetches English POIs', async () => {
      const data = await walibiHolland.FetchPOIs('en');
      const attractions = data.entertainment;
      assert(attractions.every((poi: any) => assertPOI(poi)));
    });
    it('Fetches German POIs', async () => {
      const data = await walibiHolland.FetchPOIs('de');
      const attractions = data.entertainment;
      assert(attractions.every((poi: any) => assertPOI(poi)));
    });
  });

export default fetchTests;
