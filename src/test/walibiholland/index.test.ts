/* eslint-disable @typescript-eslint/no-explicit-any */
import fetchTests from './fetch.test';
import processTests from './process.test';

const walibiHollandTest = (): any =>
  describe('Walibi Holland', () => {
    fetchTests();
    processTests();
  });

export default walibiHollandTest;
